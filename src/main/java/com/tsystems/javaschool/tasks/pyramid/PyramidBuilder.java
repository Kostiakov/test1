package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid (List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int sizeList = inputNumbers.size();
        int k = 1;
        int count = 0;
        int add = 2;
        int pos = 0;
        while (k < sizeList) {
            k = k + add;
            add++;
            count++;
        }
        if (k != sizeList) {
            throw new CannotBuildPyramidException();
        }
        int max = inputNumbers.get(0);
        for (int sch = 0; sch < inputNumbers.size(); sch++) {
            if (inputNumbers.get(sch) == null) {
                throw new CannotBuildPyramidException();
            }
            if (inputNumbers.get(sch) > max) {
                max = inputNumbers.get(sch);
            }
        }
        if (count<0) {
                throw new CannotBuildPyramidException();
            }

        int[][] array = new int[count + 1][2 * count + 1];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (j == count - i) {
                    for (int n = 0; n < i + 1; n++) {
                        int temp1 = inputNumbers.get(0);
                        int index = 0;
                        for (int sch = 0; sch < inputNumbers.size(); sch++) {
                            if (inputNumbers.get(sch) < temp1) {
                                temp1 = inputNumbers.get(sch);
                                index = sch;
                            }
                        }
                        array[i][j + 2 * n] = temp1;
                        inputNumbers.set(index, max);
                        pos++;
                    }
                }
            }
        }
        return array;
    }

}
