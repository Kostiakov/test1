package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement == "") { // check that statement not null
            return null;
        }
        for (int i = 1; i < statement.length(); i++) { // check that statement correct
            if (statement.charAt(i) == statement.charAt(i - 1) && !Character.isDigit(statement.charAt(i))) {
                return null;
            }
        }

        int open = 0; // check if there are parenthesis
        int close = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                open++;
            }
            if (statement.charAt(i) == ')') {
                close++;
            }
            if (open < close || statement.charAt(i) == ',') { // also check comma in statement
                return null;
            }
        }
        if (open != close) { // check number of opened and closed parenthesis
            return null;
        }

        if (open > 0) { // if there are parenthesis then at first calculate the result there
            int first = statement.indexOf('(');
            int last = statement.indexOf(')');
            String fragment = statement.substring(first + 1, last);
            String[] arraycalc = fragment.split("[-+*/]");

            // array of numbers
            double[] el = new double[arraycalc.length];
            for (int i = 0; i < arraycalc.length; i++) {
                el[i] = Double.valueOf(arraycalc[i]);
            }
            // array of operations
            char[] array = new char[el.length - 1];
            int arraypos = 0;
            for (int i = 0; i < fragment.length(); i++) {
                if (fragment.charAt(i) == '+' || fragment.charAt(i) == '-' || fragment.charAt(i) == '/' || fragment.charAt(i) == '*') {
                    array[arraypos] = fragment.charAt(i);
                    arraypos++;
                }
            }

            int len = array.length;

            // calculation
            for (int j = 0; j <= len - 1; j++) { // at first calculate operations "/" and "*"
                if (j == array.length) {
                    break;
                }

                if (array.length == 0) {
                    break;
                }
                if (array[j] == '/') {
                    el[j] = el[j] / el[j + 1];
                    el = decrementdoublearray(j, el);
                    array = decrementchararray(j, array);
                    j--;
                    continue;
                }

                if (array[j] == '*') {
                    el[j] = el[j] * el[j + 1];
                    el = decrementdoublearray(j, el);
                    array = decrementchararray(j, array);
                    j--;
                    continue;
                }

                if (array[j] == array.length) {
                    break;
                } else {
                    continue;
                }
            }

            for (int j = 0; j <= len - 1; j++) { // then calculate operations "+" and "-"
                if (j == array.length) {
                    break;
                }

                if (array.length == 0) {
                    break;
                }

                if (array[j] == '+') {
                    el[j] = el[j] + el[j + 1];
                    el = decrementdoublearray(j, el);
                    array = decrementchararray(j, array);
                    j--;
                    continue;
                }

                if (array[j] == '-') {
                    el[j] = el[j] - el[j + 1];
                    el = decrementdoublearray(j, el);
                    array = decrementchararray(j, array);
                    j--;
                    continue;
                }
            }
            statement = statement.replace(statement.substring(first, last + 1), String.valueOf(el[0]));
        }

        int minus = -1; // if the sum in parenthesis < 0, remember position of this element and delete "-" in order to make statement.split("[-+*/]")
        boolean change = false;
        if (open > 0) {
            int newarraypos = 0;
            for (int i = 0; i < statement.length(); i++) {
                if (statement.charAt(i) == '+' || statement.charAt(i) == '-' || statement.charAt(i) == '/' || statement.charAt(i) == '*') {
                    newarraypos++;
                    if (statement.charAt(i - 1) == '+' || statement.charAt(i - 1) == '-' || statement.charAt(i - 1) == '/' || statement.charAt(i - 1) == '*') {
                        minus = newarraypos;
                        change = true;
                        statement = statement.substring(0, newarraypos + 1) + statement.substring(newarraypos + 2, statement.length());
                        break;
                    }
                }
            }
        }

        String[] arraycalc = statement.split("[-+*/]");

        // array of numbers
        double[] el = new double[arraycalc.length];
        for (int i = 0; i < arraycalc.length; i++) {
            el[i] = Double.valueOf(arraycalc[i]);
        }

        if (change) { // return "-"
            el[minus - 1] = 0 - el[minus - 1];
        }

        // array of operations
        char[] array = new char[el.length - 1];
        int arraypos = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '+' || statement.charAt(i) == '-' || statement.charAt(i) == '/' || statement.charAt(i) == '*') {
                array[arraypos] = statement.charAt(i);
                arraypos++;
            }
        }
        for (char chr1 : array) {
            System.out.println(chr1);
        }

        // calculation
        for (int j = 0; j <= array.length - 1; j++) { // same logic as before
            if (j == array.length) {
                break;
            }

            if (array.length == 0) {
                break;
            }
            if (array[j] == '/') {
                if (el[j + 1] == 0) {
                    return null;
                }
                el[j] = el[j] / el[j + 1];
                el = decrementdoublearray(j, el);
                array = decrementchararray(j, array);
                j--;
                continue;
            }

            if (array[j] == '*') {
                el[j] = el[j] * el[j + 1];
                el = decrementdoublearray(j, el);
                array = decrementchararray(j, array);
                j--;
                continue;
            }

            if (array[j] == array.length) {
                break;
            } else {
                continue;
            }
        }

        for (int j = 0; j <= array.length - 1; j++) {
            if (j == array.length) {
                break;
            }

            if (array.length == 0) {
                break;
            }

            if (array[j] == '+') {
                el[j] = el[j] + el[j + 1];
                el = decrementdoublearray(j, el);
                array = decrementchararray(j, array);
                j--;
                continue;
            }

            if (array[j] == '-') {
                el[j] = el[j] - el[j + 1];
                el = decrementdoublearray(j, el);
                array = decrementchararray(j, array);
                j--;
                continue;
            }
        }
        
        
        if (el[0] % 1 == 0) { 
            int result = 0;
            result = (int) el[0];
            return String.valueOf(result); // return result if natural number
        }
        return String.valueOf(el[0]); // return result
    }

    public static double[] decrementdoublearray(int j1, double[] e11) {
        double[] current = new double[e11.length - 1];
        for (int k = j1 + 1; k < e11.length - 1; k++) {
            e11[k] = e11[k + 1];
        }
        for (int k = 0; k < e11.length - 1; k++) {
            current[k] = e11[k];
        }
        e11 = current;
        return e11;
    }

    public static char[] decrementchararray(int j1, char[] array1) {
        char[] current1 = new char[array1.length - 1];
        for (int k = j1; k < array1.length - 1; k++) {
            array1[k] = array1[k + 1];
        }
        for (int k = 0; k < array1.length - 1; k++) {
            current1[k] = array1[k];
        }
        array1 = current1;
        return array1;
    }

}
